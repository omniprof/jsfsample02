package com.kenfogel.quizbeans;

import java.io.Serializable;
import java.util.ArrayList;

import javax.inject.Named;
import javax.enterprise.context.SessionScoped;

/**
 * This is a CDI managed data bean whose methods are actually doing business
 * processing. This is not the best practices but it does help us understand a
 * little more about these beans. In the end we will use action beans for
 * business processes.
 *
 * @author Ken
 * @version 1.0
 *
 */
@Named
@SessionScoped
public class QuizBean implements Serializable {

    private static final long serialVersionUID = -5696075206135950369L;

    private ArrayList<ProblemBean> problems = new ArrayList<>();
    private int currentIndex;
    private int score;

    /**
     * Each problem is a sequence of numbers. The user must guess what the next
     * number in the sequence is
     */
    public QuizBean() {
        System.out.println("QuizBean Constructor");
        problems.add(
                new ProblemBean(new int[]{3, 1, 4, 1, 5}, 9)); // pi
        problems.add(
                new ProblemBean(new int[]{1, 1, 2, 3, 5}, 8)); // fibonacci
        problems.add(
                new ProblemBean(new int[]{1, 4, 9, 16, 25}, 36)); // squares
        problems.add(
                new ProblemBean(new int[]{2, 3, 5, 7, 11}, 13)); // primes
        problems.add(
                new ProblemBean(new int[]{1, 2, 4, 8, 16}, 32)); // powers of 2
    }

    public void setProblems(ArrayList<ProblemBean> newValue) {
        problems = newValue;
        currentIndex = 0;
        score = 0;
    }

    public int getScore() {
        return score;
    }

    public ProblemBean getCurrent() {
        return problems.get(currentIndex);
    }

    public String getAnswer() {
        return "";
    }

    public void setAnswer(String newValue) {
        try {
            int answer = Integer.parseInt(newValue.trim());
            if (getCurrent().getSolution() == answer) {
                score++;
            }
            currentIndex = (currentIndex + 1) % problems.size();
        } catch (NumberFormatException ex) {
            System.out.println("Error converting string to a value\n" + ex.getMessage());
        }
    }
}
