package com.kenfogel.quizbeans;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * This is an ordinary bean that will be instantiated by the CDI managed QuizBean.
 * 
 * @author Ken
 * @version 1.0
 *
 */
public class ProblemBean implements Serializable {

	private static final long serialVersionUID = -8263450944127228631L;

	private ArrayList<Integer> sequence;
	private int solution;

	public ProblemBean(int[] values, int solution) {
		sequence = new ArrayList<>();
		for (int i = 0; i < values.length; i++) {
			sequence.add(values[i]);
		}
		this.solution = solution;
	}

	public ArrayList<Integer> getSequence() {
		return sequence;
	}

	public void setSequence(ArrayList<Integer> newValue) {
		sequence = newValue;
	}

	public int getSolution() {
		return solution;
	}

	public void setSolution(int newValue) {
		solution = newValue;
	}
}
